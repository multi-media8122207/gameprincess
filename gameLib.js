function Frog() {
    tFrog = new Sprite(scene, "frog.png", 50, 60 );
    tFrog.setSpeed(0);
    tFrog.setAngle(0);

    tFrog.motionFrong = function() {
        if (keysDown[K_LEFT] || keysDown[K_A] ) {
            this.changeAngleBy(-5);
            }
        if (keysDown[K_RIGHT] || keysDown[K_D]) {
            this.changeAngleBy(5);
            }
        if (keysDown[K_UP] || keysDown[K_W] && this.speed <=10) {
            this.changeSpeedBy(1);
            }
        if (keysDown[K_DOWN] || keysDown[K_S] && this.speed >=-3) {
            this.changeSpeedBy(-1);
            }
        if (keysDown[K_SPACE] && this.speed >= 0){
            this.changeSpeedBy(-1);
            }
    }
    return tFrog;
}

function Fly() {
    tfly = new Sprite(scene, "fly.png", 10,20);

    tfly.motionFly = function() {
        newDir = (Math.random() * 90)-45;
        this.changeAngleBy(newDir);
        this.setSpeed(8);
    }

    tfly.dead = function() {
        newX = Math.random()*this.cWidth;
        newY = Math.random()*this.cHeight;
        this.setPosition(newX,newY);
    }


    

    return tfly;
}